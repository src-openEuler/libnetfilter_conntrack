#needsrootforbuild
Name:           libnetfilter_conntrack
Version:        1.0.9
Release:        4
Summary:        A userspace library providing a API
License:        GPLv2+
URL:            http://netfilter.org
Source0:        http://netfilter.org/projects/libnetfilter_conntrack/files/%{name}-%{version}.tar.bz2

Patch0:         enable-make-check-tests.patch

BuildRequires:  pkgconfig kernel-headers
BuildRequires:  libmnl-devel >= 1.0.3 libnfnetlink-devel >= 1.0.1
BuildRequires:  make

%description
libnetfilter_conntrack is a userspace library providing a programming
interface (API) to the in-kernel connection tracking state table. This
library is currently used by conntrack-tools among many other applications.

%package        devel
Summary:        The development files for libnetfilter_conntrack
BuildRequires:  gcc
Requires:       kernel-headers libnfnetlink-devel >= 1.0.1
Requires:       %{name} = %{version}-%{release}

%description    devel
This package contains development files.


%prep
%autosetup -p1

%build
%configure
%make_build

%install
%make_install
%delete_la

%check
make check

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc COPYING
%{_libdir}/*.so.*

%files devel
%{_includedir}/libnetfilter_conntrack/*.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so

%changelog
* Thu Jan 19 2023 zhangdaolong <dlzhangak@isoftstone.com> - 1.0.9-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Remove unrecognized options

* Mon Nov 28 2022 xingwei <xingwei14@h-partners.com> - 1.0.9-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add make as BuildRequires for rpmbuild

* Thu Sep 22 2022 gaihuiying <eaglegai@163.com> - 1.0.9-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:enable test

* Sun Jun 12 2022 YukariChiba <i@0x7f.cc> - 1.0.9-1
- Type:update
- ID:NA
- SUG:NA
- DESC:Upgrade version to 1.0.9

* Thu May 27 2021 lijingyuan <lijingyuan3@huawei.com> - 1.0.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add the compilation dependency of gcc.

* Wed Jul 22 2020 zhangxingliang <zhangxingliang3@huawei.com> - 1.0.8-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 1.0.8

* Mon Sep 16 2019 Yiru Wang <wangyiru1@huawei.com> - 1.0.6-7
- Pakcage init
